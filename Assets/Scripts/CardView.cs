﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;



/// <summary>
/// Card view - sits on the card prefab - used for visualization of a card and receiving click events on it 
/// </summary>
public class CardView : MonoBehaviour
{

    GameObject m_shadow;
    GameObject m_card;

    RectTransform m_rect_transform;

    Card m_card_model;

    bool m_drag = false;
    bool m_drag_snapped = false;
    bool m_active = false;
    bool m_interactable = true;

    Vector3 m_startPos = new Vector3(0, 0, 0);
    Vector3 m_org_pos = new Vector3(0, 0, 0);

    float m_org_x = 0;
    Vector2 m_org_local_pos = new Vector2();
    float m_org_z = 0;

    float combinedX;
    float combinedY;

    public int m_child_index = 0;

    bool card_thrown = false;

    Coroutine m_scale_routine=null;
    Coroutine m_move_routine = null;
    Coroutine m_rotate_routine = null;

    public float Org_x { get => m_org_x; set => m_org_x = value; }
    public RectTransform Rect_transform { get => m_rect_transform; set => m_rect_transform = value; }

    // Use this for initialization
    void Awake()
    {
        m_rect_transform = GetComponent<RectTransform>();
        m_card = this.gameObject;
        m_shadow = m_card.transform.GetChild(0).gameObject;
    }
     

    public void SetCardButton(bool enable)
    {
        if (enable)
            transform.Find("Card").GetComponent<Image>().color = new Color(1f, 1f, 1f);
        else
            transform.Find("Card").GetComponent<Image>().color = new Color(0.65f, 0.65f, 0.65f);

        m_active = enable;
    }



    public void ShowHideShadow(bool show)
    {
        if(this!=null &&  gameObject!=null)
            m_shadow.SetActive(show);
       
    }

  
    /// <summary>
    /// Deletes the card game object (visuallY)
    /// </summary>
    public void DeleteCard()
    {
        if (gameObject != null)
            Destroy(gameObject);
    }

    public void OnMouseDown()
    {
        if (m_interactable)
        {
            if (m_active)
            {
                m_org_z = m_rect_transform.localEulerAngles.z;
                //RoundController.Instance.PlayerClickedCard (m_card);
                StartDrag();
            }
        }

    }

    public void StartDrag()
    {
        if (m_interactable)
        {
            combinedX = HandController.Instance.Scaler_x / HandController.Instance.Scaler_width;
            combinedY = HandController.Instance.Scaler_y / HandController.Instance.Scaler_height;

#if (!UNITY_EDITOR && !UNITY_WEBGL)
                if (Input.touchCount > 0)
            {
    				m_startPos = new Vector3(Input.GetTouch(0).position.x*combinedX,Input.GetTouch(0).position.y*combinedY,0);
                    m_org_x = m_startPos.x;
                    SaveMyCurrLocalPosition();
                    StartMyDrag();
            }
#else
           

            m_startPos = new Vector3(Input.mousePosition.x * combinedX, Input.mousePosition.y * combinedY, 0);
            m_org_x = m_startPos.x;
            SaveMyCurrLocalPosition();
            StartMyDrag();
            
#endif
        }
    }

    private void StartMyDrag()
    {
        m_drag = true;
        ShowHideShadow(!m_drag);

        Vector2 angle_posy = HandController.Instance.Get_RotZ_And_PosY((float)HandController.Instance.GetCardPosition(m_child_index));
        RectTransform rectTransform = HandController.Instance.Cards_view[m_child_index].gameObject.GetComponent<RectTransform>();
        rectTransform.localEulerAngles = new Vector3(0, 0, angle_posy.x);
        MoveMe(new Vector3(rectTransform.localPosition.x, angle_posy.y + rectTransform.up.normalized.y * HandController.Y_ELEVATION, HandController.Instance.Cards_view[m_child_index].GetMyDepth()), m_child_index,0.05f, () =>
          {
              m_drag_snapped = true;
          });

    }

    public void SaveMyCurrLocalPosition()
    {
        m_org_local_pos = m_rect_transform.localPosition;
        m_org_z = m_rect_transform.localEulerAngles.z;
        ScaleMe(HandController.CARD_SCALE * 1.25f);
        HandController.Instance.MoveNeighbourCardsUp(true,m_child_index);
    }

   
    public void UpdateNameText()
    {
        gameObject.name = m_card_model.Rank.ToString() + m_card_model.Suit.ToString() + "_" + m_child_index;
    }
   

    public void ClickThrowCard()
    {
        //RoundController.Instance.DragEnded(200f, m_org_pos, GetComponent<CardView>(), m_org_z);

        DragEnded();
        
    }

    private void DragEnded()
    {
        m_drag_snapped = false;
        m_drag = false;
        ShowHideShadow(!m_drag);
        HandController.Instance.DragEnded(m_child_index);
    }

    public float GetMyDepth()
    {
        return (HandController.MAX_CARDS - m_child_index) * 0.1f;
    }

    void Update()
    {

        if (m_interactable)
        {
            if (m_drag)
            {

#if (!UNITY_EDITOR && !UNITY_WEBGL)
                    if (Input.touchCount > 0) {
                        m_rect_transform.localPosition -= m_startPos-new Vector3(Input.GetTouch(0).position.x*combinedX,Input.GetTouch(0).position.y*combinedY,0);
    					m_startPos =new Vector3(Input.GetTouch(0).position.x*combinedX,Input.GetTouch(0).position.y*combinedY,0);

    					HandController.Instance.CheckAndSetMyDepth(Input.mousePosition.x * combinedX, m_org_x,this, m_org_local_pos,m_org_z);

                    if(m_drag_snapped)
                        HandController.Instance.MoveCardUpdate(m_child_index);
                    }
#else
                m_rect_transform.localPosition -= m_startPos - new Vector3(Input.mousePosition.x * combinedX, Input.mousePosition.y * combinedY, 0);
                m_startPos = new Vector3(Input.mousePosition.x * combinedX, Input.mousePosition.y * combinedY, 0);

                
                HandController.Instance.CheckAndSetMyDepth(Input.mousePosition.x * combinedX, m_org_x,this, m_org_local_pos,m_org_z);

                if(m_drag_snapped)
                    HandController.Instance.MoveCardUpdate(m_child_index);
             

#endif
            }

            if (m_drag)
            {
#if (!UNITY_EDITOR && !UNITY_WEBGL)
                    if (Input.touchCount > 0) {
    					Touch	touch = Input.GetTouch(0);

    					if(touch.phase == TouchPhase.Ended)
    					{
    						DragEnded();
    					}
                    }
#else
                if (Input.GetMouseButtonUp(0))
                {
                    DragEnded();
                }
#endif
            }
        }
    }


    public void RotateMe(float rot_z,float rot_x=0, float time = HandController.TRANSLATE_TIME)
    {
        if (m_rotate_routine != null)
            StopCoroutine(m_rotate_routine);

        m_rotate_routine = StartCoroutine(RotateTo(rot_z,rot_x, time ));
    }

    public void MoveMe(Vector3 target,int index,float time= HandController.TRANSLATE_TIME, Action move_done=null,bool auto_depth=true)
    {
        if (m_move_routine != null)
            StopCoroutine(m_move_routine);

        m_move_routine = StartCoroutine(MoveTo(target, time, true, move_done, auto_depth));    
    }

    public void ScaleMe(float target_scale,float time = HandController.TRANSLATE_TIME)
    {
        Vector3 temp_scale = new Vector3(target_scale, target_scale, 1);
       
        if (m_scale_routine != null)
            StopCoroutine(m_scale_routine);

        m_scale_routine = StartCoroutine(ScaleTo(target_scale,time ));

    }

    public IEnumerator MoveTo(Vector3 endPosition, float move_time, bool animate = true, Action move_done=null, bool auto_depth=true)  //used to re arrange the ahnd after a card was thrown
    {
        float f_start_time = Time.time;
        float time_to_move = move_time;         

        Vector3 temp_position;

        RectTransform card_rect = GetComponent<RectTransform>();

        Vector3 start_pos = new Vector3(card_rect.localPosition.x, card_rect.localPosition.y,endPosition.z);
        
        if (animate)
        {

            float time_counter = 0;
            while (time_counter < 1f)
            {
                time_counter = (Time.time - f_start_time) / time_to_move;
                temp_position = new Vector3(Mathf.SmoothStep(start_pos.x, endPosition.x, time_counter), Mathf.SmoothStep(start_pos.y, endPosition.y, time_counter), Mathf.SmoothStep(start_pos.z, endPosition.z, time_counter));
                card_rect.localPosition = temp_position;
                yield return null;
            }

        }
        //backup only to ensure ending in the right location even with timing issues
        if(auto_depth)
            card_rect.localPosition  = new Vector3(endPosition.x, endPosition.y,GetMyDepth());
        else
            card_rect.localPosition = new Vector3(endPosition.x, endPosition.y, endPosition.z);

        move_done?.Invoke();

    }

    public IEnumerator ScaleTo(float endScale, float move_time, bool animate = true)  //used to re arrange the ahnd after a card was thrown
    {
        float f_start_time = Time.time;
        float time_to_move = move_time;         //card contraction time

        Vector3 temp_scale = new Vector3();

        RectTransform card_rect = GetComponent<RectTransform>();
        float start_scale = card_rect.localScale.x;
        
        if (animate)
        {
            float time_counter = 0;
            while (time_counter < 1f)
            {
                time_counter = (Time.time - f_start_time) / time_to_move;
                temp_scale = new Vector3(Mathf.SmoothStep(start_scale, endScale, time_counter), Mathf.SmoothStep(start_scale, endScale, time_counter), 1f);

                card_rect.localScale = temp_scale;

                yield return null;
            }

        }
        temp_scale = new Vector3(endScale, endScale, 1);
        card_rect.localScale = temp_scale;

    }

    public IEnumerator RotateTo(float endRot_z,float endRot_x, float move_time, bool animate = true)  //used to re arrange the ahnd after a card was thrown
    {
        float f_start_time = Time.time;
        float time_to_move = move_time;         //card contraction time

        Vector3 temp_rotate = new Vector3();

        RectTransform card_rect = GetComponent<RectTransform>();
        float start_rot_z = card_rect.localEulerAngles.z;
        float start_rot_x = card_rect.localEulerAngles.x;

        //to avoid complete totate - find the minimal rotation
        if (start_rot_z - endRot_z>50f)
            endRot_z += 360;
       
        if(start_rot_z -endRot_z < -50f)
            endRot_z -= 360;
       
        if (animate)
        {
            float time_counter = 0;
            while (time_counter < 1f)
            {
                time_counter = (Time.time - f_start_time) / time_to_move;
                temp_rotate = new Vector3(Mathf.SmoothStep(start_rot_x, endRot_x, time_counter), 0, Mathf.SmoothStep(start_rot_z, endRot_z, time_counter));

                card_rect.localEulerAngles = temp_rotate;

                yield return null;
            }

        }

        if (endRot_z > 50f)
            endRot_z -= 360f;

        temp_rotate = new Vector3(endRot_x, 0, endRot_z);
        card_rect.localEulerAngles = temp_rotate;

    }

    public void SetScale(Vector3 size)
    {
        if (m_rect_transform != null)
            m_rect_transform.localScale = size;
    }

    public void SetPosition(Vector3 pos)
    {
        if (m_rect_transform != null)
            m_rect_transform.localPosition = pos;
    }

    public void SetRotation(Vector3 new_rotation)
    {
        if (m_rect_transform != null)
            m_rect_transform.localEulerAngles = new_rotation;
    }


    public void SetCardModel(Card card_model)
    {
        m_card_model = card_model;
        RevealCard();

    }

    public Card GetCardModel()
    {
        return m_card_model;
    }

    public void RevealCard()
    {
        if (transform.Find("Card").GetComponent<Image>() != null)
            transform.Find("Card").GetComponent<Image>().sprite = HandController.Instance.Deck_images[CardImageIndex];
    }

    private int CardImageIndex
    {
        get
        {
            switch (m_card_model.Suit)
            {
                case Card.SuitType.Diamonds:
                    return (int)m_card_model.Rank - 2;
                case Card.SuitType.Clubs:
                    return (int)m_card_model.Rank - 2 + 13;
                case Card.SuitType.Hearts:
                    return (int)m_card_model.Rank - 2 + 26;
                case Card.SuitType.Spades:
                    return (int)m_card_model.Rank - 2 + 39;
                default:
                    return 0;
            }
        }
    }

    public bool Card_thrown
    {
        get
        {
            return card_thrown;
        }
        set
        {
            card_thrown = value;
        }
    }

    public bool Active { get => m_active; set => m_active = value; }
}
