﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HandController : MonoBehaviour
{
    public static HandController Instance;

    public const float CARD_SCALE = 3.5f;
    public const int MAX_CARDS = 20;
    public const float Y_ELEVATION = 150f;
    public const float TRANSLATE_TIME = 0.15f;
    public const int RELEASE_LINE = 475;
    public const float RADIUS = 5000f;

    //drag helpers
    float m_scaler_x;
    float m_scaler_y;
    float m_scaler_width;
    float m_scaler_height;
    float m_actual_width;
    float m_actual_height;


    List<CardView> m_cards_view = new List<CardView>();
    List<Vector3> m_cards_positions = new List<Vector3>();

    [SerializeField] GameObject m_hand = null;
    [SerializeField] GameObject m_canvas = null;
    [SerializeField] GameObject m_card_prefab = null;
    [SerializeField] Sprite[] m_deck_images = null;
    [SerializeField] GameObject m_toggle_button = null;

    public int m_num_cards_to_deal = 10;

    public float Scaler_x { get => m_scaler_x; set => m_scaler_x = value; }
    public float Scaler_y { get => m_scaler_y; set => m_scaler_y = value; }
    public float Scaler_width { get => m_scaler_width; set => m_scaler_width = value; }
    public float Scaler_height { get => m_scaler_height; set => m_scaler_height = value; }
    public Sprite[] Deck_images { get => m_deck_images; set => m_deck_images = value; }
    public List<Vector3> Cards_positions { get => m_cards_positions; set => m_cards_positions = value; }
    public List<CardView> Cards_view { get => m_cards_view; set => m_cards_view = value; }

    public bool is_turn = false;

    public void Start()
    {
        ReadAndSetScalerInfo();
    }

    public void Awake()
    {
        Instance = this;

        SetToggleIndication();
    }

    private void SetToggleIndication()
    {
        if (is_turn)
            m_toggle_button.GetComponent<Image>().color = Color.green;
        else
            m_toggle_button.GetComponent<Image>().color = Color.red;
    }

    public void BUTTON_ToggleGameMode()
    {
        is_turn = !is_turn;
        SetToggleIndication();
    }

    public void BUTTON_Deal()
    {
        StartDeal();
    }

    public void BUTTON_Remove_Card()
    {
        //remove a random card
        int random = UnityEngine.Random.Range(0, m_cards_view.Count);
        DeleteCard(random);
        
    }

    private void DeleteCard(int index)
    {
        DestroyImmediate(m_cards_view[index].gameObject);

        m_cards_view.RemoveAt(index);

        MoveCardsToPositions(true, false, () =>
        {
            UpdateStartPositions();
            UpdateAllNames();
        });
    }

    private void ThrowCard(int index)
    {
        GameObject temp = (GameObject)Instantiate(m_card_prefab);
        CardView temp_cardView = temp.GetComponent<CardView>();

        temp.transform.SetParent(m_hand.transform);
        temp.transform.SetAsLastSibling();

        Vector3 Curr_Pos = m_cards_view[index].Rect_transform.localPosition;
        Vector3 Curr_Scale = m_cards_view[index].Rect_transform.localScale;
        Vector3 Curr_Rotation = m_cards_view[index].Rect_transform.localEulerAngles;
        
        temp.GetComponent<RectTransform>().localPosition = Curr_Pos;
        temp.GetComponent<RectTransform>().localEulerAngles = Curr_Rotation;
        temp.GetComponent<RectTransform>().localScale = Curr_Scale;
        temp_cardView.SetCardModel(m_cards_view[index].GetCardModel());
        
        temp_cardView.ScaleMe(2.5f,0.3f);
        temp_cardView.RotateMe(0f, 45f, 0.3f);
        temp_cardView.MoveMe(new Vector3(325f, 377f, 570f), index, 0.3f, () =>
          {
              DestroyImmediate(temp);
          },false);


        DeleteCard(index);

    }

    public void BUTTON_AddCard()
    {
        AddSingleCard(new Card(Card.SuitType.Spades, Card.RankType.Ace), m_cards_view.Count,false, () =>
           {
               UpdateStartPositions();
           });
    }

    public void BUTTON_Shuffle()
    {
        List<int> aux = new List<int>();

        for (int i = 0; i < m_cards_view.Count; i++)
            aux.Add(i);

        for (int j = 0; j < 20; j++)
        {
            for (int i = 0; i < m_cards_view.Count - 1; i++)
            {
                if (UnityEngine.Random.value < 0.5f)
                {
                    int temp = aux[i];
                    aux[i] = aux[i + 1];
                    aux[i + 1] = temp;
                }
            }
        }

        //loop over the aux - if the card in pos i already in right pos - do nothing - else move there and update all params

        List<int> swapped = new List<int>();

        for (int i = 0; i < aux.Count; i++)
        {
            if (aux[i] != m_cards_view[i].m_child_index)
            {
                m_cards_view[i].m_child_index = aux[i];
                m_cards_view[i].transform.SetSiblingIndex(aux[i]);
                m_cards_view[i].MoveMe(m_cards_positions[aux[i]], i);
                m_cards_view[i].RotateMe(m_cards_positions[aux[i]].z);
            }
        }
        
        for (int i = 0; i < m_cards_view.Count; i++)
            m_cards_view[i].transform.SetSiblingIndex(m_cards_view[i].m_child_index);

        m_cards_view.Clear();
        foreach (Transform item in m_hand.transform)
        {
            m_cards_view.Add(item.GetComponent<CardView>());
        }
      
        UpdateAllNames();
        
    }

    IEnumerator Delay(float delay, Action action)
    {
        yield return new WaitForSeconds(delay);

        action?.Invoke();
    }

    private void StartDeal()
    {
        CardsList deck = FullDeck();
        deck.Shuffle();

        CardsList hand = new CardsList();

        for (int i = 0; i < m_num_cards_to_deal; i++)
        {
            hand.Add(deck[i]);
        }


        StartCoroutine(InitialDeal(1f, hand));
    }

    public GameObject GetCardObject(int index)
    {
        return m_cards_view[index].gameObject;
    }

    public int GetNumCards()
    {
        return m_cards_view.Count;
    }

    public float? GetCardPosition(int index)
    {
        if (m_cards_view.Count <= index || index < 0)
            return null;
        else
            return m_cards_view[index].GetComponent<RectTransform>().localPosition.x;
    }

    //swaps between 2 cards - works with CheckAndSetMyDepth
    public void SwapCards(int pos1, int pos2, int swap_direction)
    {
        Debug.Log("Swap Cards Called with: " + pos1 + " " + pos2);

        CardView temp = m_cards_view[pos1];
        m_cards_view[pos1] = m_cards_view[pos2];
        m_cards_view[pos2] = temp;
        m_cards_view[pos2].m_child_index = pos1 + swap_direction;

        m_cards_view[pos1].UpdateNameText();
        m_cards_view[pos2].UpdateNameText();

    }

    private void UpdateAllNames()
    {
        for (int i = 0; i < m_cards_view.Count; i++)
        {
            m_cards_view[i].UpdateNameText();
        }
    }

    //main function when being dragged - always checking my neighbour cards to see if i pass them on the X axis - and if so handles it
    public void CheckAndSetMyDepth(float card_x_pos, float m_org_x, CardView cardView, Vector3 m_org_local_pos, float m_org_z)
    {
        //float card_x_pos = Input.mousePosition.x * combinedX;
        float delta_x = m_org_x - card_x_pos;

        int curr_child_index = cardView.m_child_index;

        if (delta_x > 0)
        {
            if (curr_child_index > 0)
            {
                float? other_card = GetCardPosition(curr_child_index - 1);

                if (other_card != null)
                {
                    if (GetCardPosition(curr_child_index) < other_card)
                    {
                        MoveNeighbourCardsUp(false, curr_child_index);
                        curr_child_index--;
                        cardView.m_child_index--;
                        cardView.gameObject.transform.SetSiblingIndex(curr_child_index);
                        SwapCards(curr_child_index, curr_child_index + 1, 1);
                        SetNewPosition(m_org_local_pos, m_org_z, curr_child_index + 1);
                        cardView.Org_x = card_x_pos;
                        cardView.SaveMyCurrLocalPosition();

                    }
                }
            }
        }
        else if (delta_x < 0)
        {
            float? other_card = GetCardPosition(curr_child_index + 1);

            if (other_card != null)
            {
                if (GetCardPosition(curr_child_index) > other_card)
                {
                    MoveNeighbourCardsUp(false, curr_child_index);
                    curr_child_index++;
                    cardView.m_child_index++;
                    cardView.gameObject.transform.SetSiblingIndex(curr_child_index);
                    SwapCards(curr_child_index, curr_child_index - 1, -1);
                    SetNewPosition(m_org_local_pos, m_org_z, curr_child_index - 1);
                    cardView.Org_x = card_x_pos;
                    cardView.SaveMyCurrLocalPosition();

                }
            }
        }
    }

    //moves up and down the neighbours of the dragged card with integrity checks
    public void MoveNeighbourCardsUp(bool move_up, int child_index)
    {
        if (move_up)
        {
            AdjustCardElevation(child_index - 1, 1);
            AdjustCardElevation(child_index + 1, 1);
        }
        else
        {
            AdjustCardElevation(child_index - 1, 0);
            AdjustCardElevation(child_index + 1, 0);
        }
    }

    //moves and rotates to new position
    private void SetNewPosition(Vector2 pos, float rot_z, int index)
    {
        Vector3 vect = new Vector3(pos.x, pos.y, m_cards_view[index].GetMyDepth());
        m_cards_view[index].MoveMe(vect, index);
        m_cards_view[index].RotateMe(rot_z);
     
    }

    //moves cards partly on the Y for arch like drag
    private void AdjustCardElevation(int index, int direction)
    {
        if (index >= 0 && index < GetNumCards())
        {
            RectTransform rectTransform = GetCardObject(index).GetComponent<RectTransform>();
            Vector3 vector3 = Cards_positions[index];

            //moving the card back to its normal Y position - need to adjust the X - as its a translated X
            if (direction == 0)
            {
                m_cards_view[index].MoveMe(new Vector3(vector3.x, vector3.y, Cards_view[index].GetMyDepth()), index);
                m_cards_view[index].ScaleMe(CARD_SCALE);
            }
            //this is for moving the cards up - by ELEVATION/3 
            else
            {
                m_cards_view[index].MoveMe(new Vector3(vector3.x + rectTransform.up.normalized.x * Y_ELEVATION / 3, vector3.y + rectTransform.up.normalized.y * Y_ELEVATION / 3, Cards_view[index].GetMyDepth()), index);
                m_cards_view[index].ScaleMe(CARD_SCALE * 1.1f);
            }

            m_cards_view[index].RotateMe(vector3.z);
        }

    }

    public void DragEnded(int child_index)
    {
        //add a check to see if its my turn - and if so - if my Y is bigger then some limit i release (delete) the card
        MoveNeighbourCardsUp(false, child_index);
        AdjustCardElevation(child_index, 0);
    }

    //called form the card view on each frame that the card is being dragged
    public void MoveCardUpdate(int child_index)
    {
        Vector2 angle_posy = Get_RotZ_And_PosY((float)GetCardPosition(child_index));
        RectTransform rectTransform = m_cards_view[child_index].gameObject.GetComponent<RectTransform>();
        rectTransform.localEulerAngles = new Vector3(0, 0, angle_posy.x);

        if(is_turn)
        {
            rectTransform.localPosition = new Vector3(rectTransform.localPosition.x, rectTransform.localPosition.y, m_cards_view[child_index].GetMyDepth());

            if(rectTransform.localPosition.y>RELEASE_LINE)
            {
                ThrowCard(child_index);
                //DeleteCard(child_index);
            }
        }
        else
            rectTransform.localPosition = new Vector3(rectTransform.localPosition.x, angle_posy.y + rectTransform.up.normalized.y * Y_ELEVATION, m_cards_view[child_index].GetMyDepth());
    }

    //loops over all the cards and moves to their positions based on their indexes
    private void MoveCardsToPositions(bool animate = true, bool is_deal = false, Action move_done = null)
    {
        Vector2 center = new Vector2(0, 0);
        float radius = RADIUS;//this is the diameter of the big circle
        int number_of_cards = m_cards_view.Count;
        float spread = 15f;//this is the part of the huge circle that we align our cards on - this is in angle units
        //this makes the hand smalled when we have more cards

        if (number_of_cards == 11)
            radius = RADIUS*1.1f;
        
        if (is_deal)
            spread = number_of_cards * 20 / 13;
        else
        {
            if (number_of_cards <= 2)
                spread = 3f;
            else 
                spread = 15f;
            
        }


        for (int i = 0; i < number_of_cards; i++)
        {
            float new_y_pos = 0, new_x_pos = 0, new_curve = 0;
            float curr_angle = 0;

            if (number_of_cards > 1)//to avoid division by 0
                curr_angle = (spread / (number_of_cards - 1)) * i - spread / 2;

            curr_angle += 90f;
            new_y_pos = Mathf.Sin(curr_angle * Mathf.Deg2Rad) * radius - radius;
            new_x_pos = Mathf.Cos(curr_angle * Mathf.Deg2Rad) * radius;
            new_curve = curr_angle - 90f;

            try
            {
                //the Z depth is to improve the selection of correct card
                Vector3 endPos = new Vector3(new_x_pos, new_y_pos, 0);
                StartCoroutine(ArrangeHandAnimation(m_cards_view[number_of_cards - 1 - i].GetComponent<CardView>(), endPos, new_curve, 0.2f, animate, move_done));
            }
            catch (Exception)
            {
                // This coroutine sometimes throws an exception
            }

        }

    }


    //for allowing a card to be drag on an arch - the pos_x gets back a Y and rotation
    public Vector2 Get_RotZ_And_PosY(float pos_x)
    {
        Vector2 center = new Vector2(0, 0);
        float radius = RADIUS;//this is the diameter of the big circle
        int number_of_cards = m_cards_view.Count;
        float spread = 15f;//this is the part of the huge circle that we align our cards on - this is in angle units
                           //this makes the hand smalled when we have more cards

        if (number_of_cards == 11)
            radius = RADIUS * 1.1f;

        
        if (number_of_cards <= 2)
            spread = 3f;
        else 
            spread = 15f;
        

        float new_y_pos = 0, new_x_pos = 0, new_curve = 0;
        float curr_angle = 0;

        curr_angle += 90f;
        new_y_pos = Mathf.Sin(curr_angle * Mathf.Deg2Rad) * radius - radius;
        new_x_pos = Mathf.Cos(curr_angle * Mathf.Deg2Rad) * radius;
        new_curve = curr_angle - 90f;

        float new_angle = Mathf.Acos(pos_x / radius) * Mathf.Rad2Deg;
        float new_y = Mathf.Sin(new_angle * Mathf.Deg2Rad) * radius - radius;

        return new Vector2(new_angle - 90f, new_y);

    }

    

    /// full translate of card
    public IEnumerator ArrangeHandAnimation(CardView card, Vector3 endPosition, float endRotation, float move_time, bool animate = true, Action arrange_done = null)  //used to re arrange the ahnd after a card was thrown
    {
        float f_start_time = Time.time;
        float time_to_move = move_time;         //card contraction time

        Vector3 temp_position;
        Vector3 temp_scale;
        Vector3 temp_rotation;
        CardView temp_cardview;

        temp_cardview = card;

        float curr_angle = temp_cardview.Rect_transform.localEulerAngles.z;
        float curr_angle_x = temp_cardview.Rect_transform.localEulerAngles.x;
        
        float start_pos_x = temp_cardview.Rect_transform.localPosition.x;
        float start_pos_y = temp_cardview.Rect_transform.localPosition.y;
        float start_pos_z = temp_cardview.Rect_transform.localPosition.z;

        float start_scale = temp_cardview.Rect_transform.localScale.x;


        float target_x = endPosition.x;
        float target_y = endPosition.y;
        float target_z = card.GetMyDepth();

        if (animate)
        {

            float time_counter = 0;
            while (time_counter < 1f)
            {
                float vel2 = 0f;

                float vel3 = 2f;

                time_counter = (Time.time - f_start_time) / time_to_move;

                temp_position = new Vector3(Mathf.SmoothStep(start_pos_x, target_x, time_counter), Mathf.SmoothStep(start_pos_y, target_y, time_counter), Mathf.SmoothStep(start_pos_z, target_z, time_counter));

                temp_scale = new Vector3(Mathf.SmoothStep(start_scale, CARD_SCALE, time_counter), Mathf.SmoothStep(start_scale, CARD_SCALE, time_counter), 1f);

                temp_cardview.Rect_transform.localScale = temp_scale;

                temp_cardview.Rect_transform.localPosition = temp_position;

                curr_angle = Mathf.SmoothDampAngle(curr_angle, endRotation, ref vel2, 0.1f);

                curr_angle_x = Mathf.SmoothDampAngle(curr_angle_x, 0, ref vel3, 0.1f);

                temp_rotation = new Vector3(curr_angle_x, 0, curr_angle);

                temp_cardview.SetRotation(temp_rotation);

                yield return null;
            }

        }

        temp_cardview.SetRotation(new Vector3(0, 0, endRotation));               //backup only to ensure ending in the right location even with timing issues
        temp_cardview.SetPosition(new Vector3(target_x, target_y, target_z));
        temp_cardview.SetScale(new Vector3(CARD_SCALE, CARD_SCALE, 1f));

        if (arrange_done != null)
            arrange_done();
    }

    public void AddSingleCard(Card card, int card_index,bool deal, Action move_done=null)
    {
        GameObject temp = (GameObject)Instantiate(m_card_prefab);
        m_cards_view.Add(temp.GetComponent<CardView>());

        m_cards_view[m_cards_view.Count - 1].transform.SetParent(m_hand.transform);
        m_cards_view[m_cards_view.Count - 1].transform.SetAsLastSibling();
        m_cards_view[m_cards_view.Count - 1].GetComponent<RectTransform>().localPosition = new Vector3(0f, 0f, 0);
        m_cards_view[m_cards_view.Count - 1].GetComponent<RectTransform>().localEulerAngles = new Vector3(0f, 0f, 0);
        m_cards_view[m_cards_view.Count - 1].GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
        m_cards_view[m_cards_view.Count - 1].GetComponent<CardView>().SetCardModel(card);
        m_cards_view[m_cards_view.Count - 1].GetComponent<Collider2D>().enabled = enabled;
        m_cards_view[m_cards_view.Count - 1].m_child_index = card_index;
        m_cards_view[m_cards_view.Count - 1].UpdateNameText();

        MoveCardsToPositions(true, deal, move_done);
    }

    public void ReadAndSetScalerInfo()
    {
        CanvasScaler scaler = m_canvas.GetComponent<CanvasScaler>();

        // Fix the scaler for wide screens
        // In WebGL, do not fix since might not work on full screen mode
#if !UNITY_WEBGL
        if (Camera.main.aspect > 16 / 9f)
        {
            scaler.matchWidthOrHeight = 1f;
        }
#endif

        m_scaler_x = scaler.referenceResolution.x;
        m_scaler_y = scaler.referenceResolution.y;

        m_scaler_width = Screen.width;
        m_scaler_height = Screen.height;

        m_actual_width = m_canvas.GetComponent<RectTransform>().rect.width;
        m_actual_height = m_canvas.GetComponent<RectTransform>().rect.height;

        // For wide screens, we change the canvas scaler mode in runtime. For this reason, 
        // the actual height and width must be adjusted            
        if (scaler.matchWidthOrHeight == 1)
        {
            m_actual_width = 1080 * m_actual_width / m_actual_height;
            m_actual_height = 1080;
        }

    }

    public IEnumerator InitialDeal(float total_time, CardsList cards)
    {
        int total_cards_dealt = 0;
        float delay = total_time / 13f;
        float start_time = Time.time;

        while (Time.time - start_time < total_time)
        {
            yield return new WaitForSeconds(delay);

            if (total_cards_dealt < m_num_cards_to_deal)
                AddSingleCard(cards[total_cards_dealt], total_cards_dealt,true);

            total_cards_dealt++;
        }

        yield return new WaitForSeconds(0.25f);
        UpdateStartPositions();
         
        
    }

    //m_cards_positions holds the positions of each card based on index and on number of cards in hand
    //needs to be updated each time you add or remove a card
    private void UpdateStartPositions()
    {
        m_cards_positions.Clear();

        for (int i = 0; i < m_cards_view.Count; i++)
        {
            RectTransform rectTransform = m_cards_view[i].GetComponent<RectTransform>();
            m_cards_positions.Add(new Vector3(rectTransform.localPosition.x, rectTransform.localPosition.y, rectTransform.localEulerAngles.z));
            m_cards_view[i].m_child_index = i;
            m_cards_view[i].Active = true;
        }
    }

    //builds a full standard deck
    public CardsList FullDeck()
    {
        CardsList result = new CardsList();

        foreach (Card.SuitType suit in Enum.GetValues(typeof(Card.SuitType)))
        {
            foreach (Card.RankType rank in Enum.GetValues(typeof(Card.RankType)))
            {
                result.Add(new Card(suit, rank));
            }
        }
        return result;
    }

}
